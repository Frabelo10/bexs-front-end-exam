const inputCardNumber = $('.input--cardNumber');
const labelCard = $('.label--cardNumber');
const cardValidation = $('.cardNumber_alert');

inputCardNumber.on('focus', () => {
  labelCard.addClass('active');
});
inputCardNumber.on('blur', () => {
  if(inputCardNumber.val().length == 0){
  labelCard.removeClass('active');
  $('.card1').removeAttr('style');
  $('.logo').attr('style', 'display:none;');
  $('.number_card').html('**** **** **** ****');
  }
  cardNumberValidator();
});
inputCardNumber.on('keyup', () =>{
  $('.card1').attr('style', "background:transparent url('clientlibs/css/img/Group1603.png') 0% 0% no-repeat padding-box;");
  if(innerWidth >= 1024){
    $('.card1').attr('style', "background:transparent url('clientlibs/css/img/Group1603.svg') 0% 0% no-repeat padding-box;"); 
  }
  $('.logo').removeAttr('style');
  $('.number_card').html($('.input--cardNumber').val());
});
const inputHolderName = $('.input--holderName');
const labelHolder = $('.label--holderName');
const holderNameValidation = $('.holderName_alert');
inputHolderName.on('focus', () => {
  labelHolder.addClass('active');
});
inputHolderName.on('blur', () => {
  if($('.input--holderName').val().length == 0){
  labelHolder.removeClass('active');
  $('.holder_name p').html('NOME DO TITULAR');
  inputHolderName.removeAttr('style');
  holderNameValidation.attr('style','display:none;');
}
else{
  holderNameValidator(inputHolderName.val());
  }
});
inputHolderName.on('keyup', () =>{
  $('.holder_name p').html($('.input--holderName').val());
});
const inputExpiration = $('.input--expirationDate');
const labelExpiration = $('.label--expirationDate');
const expirationValidation = $('.expirationDate_alert');
inputExpiration.on('focus', () => {
  labelExpiration.addClass('active');
  monthValidator();
  yearValidator();
});
inputExpiration.on('blur', () => {
  if($('.input--expirationDate').val().length == 0){
  labelExpiration.removeClass('active');
    $('.expiration_date p').html('00/00');
}
  monthValidator();
  yearValidator();
});
inputExpiration.on('keyup', () =>{
  $('.expiration_date p').html($('.input--expirationDate').val());
});
const inputCvv = $('.input--cvv');
const labelCvv = $('.label--cvv');
const cvvValidation = $('.cvv_alert');
inputCvv.on('focus', () => {
  labelCvv.addClass('active');
  if(innerWidth >= 1024){
  $('.card1').attr('style', "background:transparent url('clientlibs/css/img/Group433.png') 0% 0% no-repeat padding-box;");
  $('.number_card').html('');
  $('.holder_name p').html('');
  $('.expiration_date p').html('');
  $('.cvv').removeAttr('style');
  $('.logo').attr('style', 'display:none;');
  }
});
inputCvv.on('blur', () => {
  if($('.input--cvv').val().length == 0){
  labelCvv.removeClass('active');}
  if(inputCvv.val().length == 0 && inputCardNumber.val().length == 0 && $('.input--holderName').val().length == 0 && $('.input--expirationDate').val().length == 0 && innerWidth >= 1024) {
    $('.card1').attr('style', "background:transparent url('clientlibs/css/img/Group 1735.png') 0% 0% no-repeat padding-box;");
    $('.expiration_date p').html('00/00');
    $('.holder_name p').html('NOME DO TITULAR'); 
    $('.number_card').html('**** **** **** ****');
    $('.cvv').attr('style', 'display:none');
  }
  else{
    $('.card1').attr('style', "background:transparent url('clientlibs/css/img/Group1603.svg') 0% 0% no-repeat padding-box;");
    $('.expiration_date p').html($('.input--expirationDate').val());
    $('.holder_name p').html($('.input--holderName').val());
    $('.number_card').html($('.input--cardNumber').val());
    $('.cvv').attr('style', 'display:none');

  }
  cvvValidator();
});
inputCvv.on('keyup', () =>{
  if(innerWidth >= 1024){
    $('.card1').attr('style', "background:transparent url('clientlibs/css/img/Group433.svg') 0% 0% no-repeat padding-box;");
    $('.cvv').html(inputCvv.val()); 
  }
});
const inputParcel = $('.input--parcelNumber');
const labelParcel = $('.label--parcelNumber');
const parcelNumberValidation = $('.parcelNumber_alert');

inputParcel.on('focus', () => {
  labelParcel.addClass('active');
  labelParcel.addClass('opacity');
});
inputParcel.on('blur', () => {
  if($('.input--parcelNumber').val() === null){
  labelParcel.removeClass('active');
  labelParcel.removeClass('opacity');}
  parcelNumberValidator();
});
$(":input").inputmask();

function cardNumberValidator(){
  var cardNum = inputCardNumber.val().replace( /\D/g, ''); 
  if(cardNum.length < 16 && cardNum.length != ''){ 
    cardValidation.removeAttr('style');
    inputCardNumber.attr('style','border-bottom: 2px solid #EB5757;');
    }
  else{
    inputCardNumber.removeAttr('style');
    cardValidation.attr('style','display:none;');
    }
};

function holderNameValidator(nome){
 
  if(!!nome.match(/[A-Z][a-z]* [A-Z][a-z]*/)){
    inputHolderName.removeAttr('style');
    holderNameValidation.attr('style','display:none;');
  }
  else{
    holderNameValidation.removeAttr('style');
    inputHolderName.attr('style','border-bottom: 2px solid #EB5757;');
    
  }
  };

function monthValidator() {
  
  if (parseInt(inputExpiration.val().slice(0,2)) < 1 || parseInt(inputExpiration.val().slice(0,2)) > 12){
    inputExpiration.attr('style','border-bottom: 2px solid #EB5757;');
    expirationValidation.removeAttr('style');
  }
  else{
    inputExpiration.removeAttr('style');
    expirationValidation.attr('style','display:none;');
  }
  };
  function yearValidator() {
    const currentYear =  new Date().getYear().toString().substr(-2);
    if (parseInt(inputExpiration.val().slice(3,5)) < currentYear ){
      inputExpiration.attr('style','border-bottom: 2px solid #EB5757;');
      expirationValidation.removeAttr('style');
    }
    else{
      inputExpiration.removeAttr('style');
      expirationValidation.attr('style','display:none;');
    }
    const currentMonth = new Date().getMonth();
    if(parseInt(inputExpiration.val().slice(3,5)) == currentYear && currentMonth > parseInt(inputExpiration.val().slice(0,2)) && parseInt(inputExpiration.val().slice(0,2)) <= 12 ){
      inputExpiration.attr('style','border-bottom: 2px solid #EB5757;');
      expirationValidation.removeAttr('style');
    }
    };

function cvvValidator(){
  var cvvNum = inputCvv.val().replace( /\D/g, ''); 
  if(cvvNum.length < 3 && cvvNum.length != ''){ 
    cvvValidation.removeAttr('style');
    inputCvv.attr('style','border-bottom: 2px solid #EB5757;');
    }
  else{
    inputCvv.removeAttr('style');
    cvvValidation.attr('style','display:none;');
    }
};
function parcelNumberValidator(){
    if(inputParcel.val() == null){
      parcelNumberValidation.removeAttr('style');
      inputParcel.attr('style','border-bottom: 2px solid #EB5757;');
    }
    else{
      inputParcel.removeAttr('style');
      parcelNumberValidation.attr('style','display:none;');
    }
}

if(innerWidth >= 1024){
  $('.text').html('Alterar forma de pagamento');
}